<?php
/*
Plugin Name: 			Accommodation
Description: 			Creates a custom post type displaying accommodation options. Requires Sitebooster Theme and ACF.
Version:           		1.0.0
Author:            		Glyn Harrison
License:           		GNU General Public License v2
License URI:       		http://www.gnu.org/licenses/gpl-2.0.html
Plugin URI: 			https://bitbucket.org/thebrandchap/plugin-accommodation/
Bitbucket Plugin URI: 	https://bitbucket.org/thebrandchap/plugin-accommodation/
*/

// Register Custom Post Type
function accom_post_type() {

	$labels = array(
		'name'                  => 'Accommodation',
		'singular_name'         => 'Accommodation',
		'menu_name'             => 'Accommodation',
		'name_admin_bar'        => 'Accommodation',
		'archives'              => 'Accommodation Archives',
		'attributes'            => 'Accommodation Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Accommodation',
		'description'           => 'Accommodation Products',
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'thumbnail', 'comments' ),
		'taxonomies'            => array( 'accommodation_tax', 'accommodation_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'accom', $args );

}
add_action( 'init', 'accom_post_type', 0 );


// Register Accommodation Categories
function accommodation_tax() {

	$labels = array(
		'name'                       => 'Accommodation Location',
		'singular_name'              => 'Accommodation Location',
		'menu_name'                  => 'Locations',
		'all_items'                  => 'All Locations',
		'parent_item'                => 'Location Group',
		'parent_item_colon'          => 'Location Group:',
		'new_item_name'              => 'New Location Name',
		'add_new_item'               => 'Add New Location',
		'edit_item'                  => 'Edit Location',
		'update_item'                => 'Update Location',
		'view_item'                  => 'View Location',
		'separate_items_with_commas' => 'Separate locations with commas',
		'add_or_remove_items'        => 'Add or remove locations',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Locations',
		'search_items'               => 'Search Locations',
		'not_found'                  => 'Location Not Found',
		'no_terms'                   => 'No Locations',
		'items_list'                 => 'Locations list',
		'items_list_navigation'      => 'Locations list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_admin_bar'          => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'accommodation_type', array( 'accom' ), $args );

}
add_action( 'init', 'accommodation_tax', 0 );




// Register Accommodation Tags
function accommodation_tags() {

	$labels = array(
		'name'                       => 'Accommodation Features',
		'singular_name'              => 'Accommodation Feature',
		'menu_name'                  => 'Features',
		'all_items'                  => 'All Features',
		'parent_item'                => 'Parent Feature',
		'parent_item_colon'          => 'Parent Feature:',
		'new_item_name'              => 'New Feature',
		'add_new_item'               => 'Add New Feature',
		'edit_item'                  => 'Edit Feature',
		'update_item'                => 'Update Feature',
		'view_item'                  => 'View Feature',
		'separate_items_with_commas' => 'Separate Features with commas',
		'add_or_remove_items'        => 'Add or remove Features',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular Features',
		'search_items'               => 'Search Features',
		'not_found'                  => 'Feature Not Found',
		'no_terms'                   => 'No Features',
		'items_list'                 => 'Features list',
		'items_list_navigation'      => 'Features list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_admin_bar'          => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'accommodation_feature', array( 'accom' ), $args );

}
add_action( 'init', 'accommodation_tags', 0 );

add_filter( 'template_include', 'template_include_accommodation', 1 );
function template_include_accommodation( $template_path ) {

	if ( get_post_type() == 'accom' ) {
		// checks if the file exists in the theme first
		if ( is_single() ) {
      if ( $theme_file = locate_template( array ( 'single-accom.php' ) ) ) {
          $template_path = $theme_file;

			// otherwise serve the file from the plugin
      } else {
          $template_path = plugin_dir_path( __FILE__ ) . 'single-accom.php';
      }

			function accom_add_footer_styles() {
				$filename = get_stylesheet_directory_uri() . '/css/accommodation/accommodation_single.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
					$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
				wp_enqueue_style( 'accom-styles', $filename );
			};
			add_action( 'get_footer', 'accom_add_footer_styles' );

	    } else if ( is_category() || is_archive() ) {

				if ( $theme_file = locate_template( array ( 'taxonomy-accommodation_type.php' ) ) ) {
					$template_path = $theme_file;
				} else {
					$template_path = plugin_dir_path( __FILE__ ) . 'taxonomy-accommodation_type.php';
				}

				function accom_add_footer_styles() {
					$filename = get_stylesheet_directory_uri() . '/css/accommodation/accommodation_tax.css';
					$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
					$filenonroot = str_replace($root,"",$filename);
					if (file_exists($filenonroot)) {
						$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
					}
					wp_enqueue_style( 'accom-styles', $filename );

					$sliderstyle = plugin_dir_url( __FILE__ ) . 'slider/nouislider.css';
					wp_enqueue_style( 'slider-style', $sliderstyle );
				};
				add_action( 'get_footer', 'accom_add_footer_styles' );


				function accom_add_footer_script() {
					$sliderscript = plugin_dir_url( __FILE__ ) . 'slider/nouislider.js';
					wp_enqueue_script( 'slider-script', $sliderscript );
					$wNumbscript = plugin_dir_url( __FILE__ ) . 'slider/wNumb.min.js';
					wp_enqueue_script( 'wNumb-script', $wNumbscript );

				};
				add_action( 'get_footer', 'accom_add_footer_script' );

			}
		}

return $template_path; }



function xmlToArray($xml, $options = array()) {
    $defaults = array(
        'namespaceSeparator' => ':',//you may want this to be something other than a colon
        'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
        'alwaysArray' => array(),   //array of xml tag names which should always become arrays
        'autoArray' => true,        //only create arrays for tags which appear more than once
        'textContent' => '$',       //key used for the text content of elements
        'autoText' => true,         //skip textContent key if node has no attributes or child nodes
        'keySearch' => false,       //optional search and replace on tag and attribute names
        'keyReplace' => false       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; //add base (empty) namespace

    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            //replace characters in attribute name
            if ($options['keySearch']) $attributeName =
                    str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
            $attributesArray[$attributeKey] = (string)$attribute;
        }
    }

    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            //recurse into child nodes
            $childArray = xmlToArray($childXml, $options);
            list($childTagName, $childProperties) = each($childArray);

            //replace characters in tag name
            if ($options['keySearch']) $childTagName =
                    str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            //add namespace prefix, if any
            if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

            if (!isset($tagsArray[$childTagName])) {
                //only entry with this key
                //test if tags of this type should always be arrays, no matter the element count
                $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                        ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                //key already exists and is integer indexed array
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                //key exists so convert to integer indexed array with previous value in position 0
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }

    //get text content of node
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    //return node as array
    return array(
        $xml->getName() => $propertiesArray
    );
}
function flatten($array, $prefix = '') {
    $result = array();
    foreach($array as $key=>$value) {
        if(is_array($value)) {
            $result = $result + flatten($value, $prefix . $key . '_');
        }
        else {
            $result[$prefix.$key] = $value;
        }
    }
    return $result;
}


add_action('wp_ajax_myfilter', 'accommodation_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_myfilter', 'accommodation_filter_function');

function accommodation_filter_function(){

	$post_keys = array_keys($_POST);

	$tag_array = preg_grep("/^tag/i", $post_keys);
	$tag_array = str_replace('tag_', '', $tag_array);

	$location_array = preg_grep("/^location/i", $post_keys);
	$location_array = str_replace('location_', '', $location_array);

	// Get a CSV of the selected locations
	$location_csv = implode(',', $location_array);
	$tag_slugs = implode('+', $tag_array);


	// Update Url and add previous page to history
	$updatedURL = '<script>history.pushState("", ';

	// Set Title
	if ($location_csv !== '') {
		$updatedURL .= '"Self Catered accommodation in '.$location_csv.' The Lake District"';
	} else if (!empty($tag_array)) {
		$updatedURL .= '"Self Catered Accommodation with ';
		$updatedURL .= implode(',', $tag_array);
		$updatedURL .= ' in The Lake District"';
	}

	$updatedURL .= ',"';

	// Start building appended URL Details

	// Adding Location and features
	if ( !empty($location_csv) || !emprty($tag_slugs) ) {

		// Append location to URL
		if (!empty($location_csv)) {
			$updatedURL .= '/accommodation_type/all/?location='.$location_csv;
		}
		// Append Feature(s) to URL
		if(!empty($tag_slugs)) {
			$updatedURL .= '?extras='.$tag_slugs;
		}
	}


	// Add arrival date
	if ( isset($_POST['arrival-date']) && $_POST['arrival-date'] !== '') {
		$updatedURL .= '?arrival='.$_POST['arrival-date'];

		$date = new DateTime( $_POST['arrival-date'] );
		$day = $date->format('l');
		// if ( $day != 'Friday' && $day != 'Saturday' ) {
		// 	echo '<div class="alert"><p class="small">Please note: <b>' . $day .'</b> is not an available checkin day for our properties. Please chose either a <b>Friday</b> or <b>Saturday</b> as your arrival date.</p></div>';
		// }
	}

	// Sort by title-date-random
	if ( isset($_POST['sortby']) && $_POST['sortby'] !== 'price' ) {

		$args = array(
			'orderby' => $_POST['sortby'],
			'order'		=> $_POST['sortorder']
		);

	// or sort by price
	} else if (isset($_POST['sortby']) && $_POST['sortby'] == 'price') {

		// Sort price acsending
		if ( $_POST['sortorder'] == 'ASC' ) {

			$args = array(
				'post_type'	=> 'accom',
				'meta_key'	=> 'min-price',
				'orderby' 	=> 'meta_value_num',
				'order'			=> 'ASC' // ASC or DESC
			);

		// Sort price descending
		} else {
			$args = array(
				'post_type'	=> 'accom',
				'orderby' 	=> 'meta_value_num',
				'meta_key' 	=> 'min-price',
				'order'			=> 'DESC' // ASC or DESC
			);
		}
	}
	if(isset($_POST['sortby'])) {
		$updatedURL .= '?sortby='.$_POST['sortby'];
	}
	if(isset($_POST['sortorder'])) {
		$updatedURL .= '?order='.$_POST['sortorder'];
	}

	if(isset($_POST['viewtype'])) {
		$updatedURL .= '?viewtype='.$_POST['viewtype'];
	};

	// Filter by both category and tags
	if( !empty( $location_array ) && !empty($tag_array) ) {
		$args['tax_query'] = array(
			'post_type' => 'accom',
			'relation' => 'AND',
			array(
				'taxonomy'					=> 'accommodation_type',
				'field' 						=> 'slug',
				'include_children'	=> 1,
				'terms' 						=> $location_array,
			),
			array(
				'taxonomy'	=> 'accommodation_feature',
				'field' 		=> 'slug',
				'terms'			=> $tag_array,
				'operator' 	=> 'AND',
			)
		);

	// Filter just by location
	} else if( !empty( $location_array ) ) {
		$args['tax_query'] = array(
			'post_type' => 'accom',
			array(
				'taxonomy'					=> 'accommodation_type',
				'include_children'	=> 1,
				'field' 						=> 'slug',
				'terms' 						=> $location_array
			)
		);

	// Filter just by tags
	} else if( $tag_array !== '' ) {
		$args['tax_query']	= array(
			'post_type' 			=> 'accom',
			array(
				'taxonomy'			=> 'accommodation_feature',
				'field' 				=> 'slug',
				'terms'					=> $tag_array,
				'operator' 			=> 'AND',
			)
		);
	}



	// Ensure Price min & max are intergers
	$_POST['price-min'] = $_POST['price-min'] + 0;
	$_POST['price-max'] = $_POST['price-max'] + 0;


	// create $args['meta_query'] array if one of the following fields is filled
	if(
		isset( $_POST['price_min'] ) && $_POST['price_min'] ||
		isset( $_POST['price_max'] ) && $_POST['price_max'] ||
		isset( $_POST['max_occupancy'] ) && $_POST['max_occupancy'] ||
		isset( $_POST['room'] ) && $_POST['room'] ) {
			$args['meta_query'] = array( 'relation'=>'AND' ); // AND means that all conditions of meta_query should be true
		}

	// if both minimum price and maximum price are specified we will use BETWEEN comparison
	if( isset( $_POST['price-min'] ) && $_POST['price-min'] && isset( $_POST['price-max'] ) && $_POST['price-max'] ) {
		$args['meta_query'][] = array(
			'relation' => 'AND',
			array (
				'key' => 'min-price',
				'value' => $_POST['price-min'],
				'type' => 'numeric',
				'compare' => '>='
			),
			array (
				'key' => 'min-price',
				'value' => $_POST['price-max'],
				'type' => 'numeric',
				'compare' => '<='
			)
		);
		// Append Min & Max price to URL
		$updatedURL .= '?max-price='.$_POST['price-max'];
		$updatedURL .= '?min-price='.$_POST['price-min'];
	} else {
		// if only min price is set
		if( isset( $_POST['price-min'] ) && $_POST['price-min'] ) {
			$args['meta_query'][] = array(
				'key' => 'min-price',
				'value' => $_POST['price-min'],
				'type' => 'numeric',
				'compare' => '>='
			);
			// Append Min price to URL
			$updatedURL .= '?min-price='.$_POST['price-min'];
		};

		// if only max price is set
		if( isset( $_POST['price-max'] ) && $_POST['price-max'] ) {
			$args['meta_query'][] = array(
				'key' => 'min-price',
				'value' => $_POST['price-max'],
				'type' => 'numeric',
				'compare' => '<='
			);
			// Append Max price to URL
			$updatedURL .= '?max-price='.$_POST['price-max'];
		}
	}

	if(isset( $_POST['max_occupancy'] ) && $_POST['max_occupancy'] ) {
		$args['meta_query'][] = array(
			'key' => 'max_occupancy',
			'value' => $_POST['max_occupancy'],
			'type' => 'numeric',
			'compare' => '>='
		);
		// Append Max occupancy to URL
		$updatedURL .= '?max-occupancy='.$_POST['max_occupancy'];
	}
	if(isset( $_POST['room'] ) && $_POST['room'] ) {
		$args['meta_query'][] = array(
			'key' => 'no_rooms',
			'value' => $_POST['room'],
			'type' => 'numeric',
			'compare' => '>='
		);
		// Append Max occupancy to URL
		$updatedURL .= '?min-rooms='.$_POST['room'];
	}

	$query = new WP_Query( $args );

	if( $query->have_posts() ) {

		// Map view
		if( isset( $_POST['viewtype'] ) && $_POST['viewtype'] == 'map') {
			include( plugin_dir_path( __FILE__ ) . '/map/multi-map.php');
		// List view
		} else {
			$grid_item_count = 1;
			echo '<div class="items" style="justify-content: center;">';
			while( $query->have_posts() ): $query->the_post();
				include(plugin_dir_path( __FILE__ ) . 'accom/card_view.php');
			endwhile;
			echo '</div>';

		}

		$updatedURL .= '");

		</script>';
		echo $updatedURL;

	// Oh oh! Nothing!
	} else {
		echo '<div class="slice text"><div class="txt_blk avs_default  avm_default normal"><div class="text_content"><h1 style="text-align: center;">Sorry, we haven&apos;t been able to match all of your choices</h1><p style="text-align: center;">Please remove some your your filters and try again</p></div></div></div>';
	}

	wp_reset_postdata();
	die();

}








/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'accom';
	$taxonomy  = 'accommodation_type';
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'accom'; // post type
	$taxonomy  = 'accommodation_type'; // taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}


// CREATE XML PAGE LISTING ALL ACCOMMODATION
add_action( 'publish_post', 'accomodation_catalogue_generator' );
add_action( 'publish_page', 'accomodation_catalogue_generator' );
add_action( 'save_post',    'accomodation_catalogue_generator' );

function accomodation_catalogue_generator() {

	$type = 'accom';
  $args = array(
    'post_type' => $type,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'ignore_sticky_posts'=> true
  );
	$my_query = new WP_Query($args);

	if( $my_query->have_posts() ) {

	  $accomm_catalogue =
			'<?xml version="1.0" encoding="UTF-8"?>' .
			'<listings>' .
		  '<title>Lakeland Retreats Self Catering Cottages</title>';

		while ($my_query->have_posts()) : $my_query->the_post();

			$map = get_field('map');
			$post_id = get_the_ID();

      $accomm_catalogue .=
				'<listing>'.
					'<hotel_id>LR_' . $post_id . '</hotel_id>' .
					'<name>' . get_the_title() . '</name>' .
					'<description>' . get_the_excerpt() . '</description>' .
			    '<brand>' . get_bloginfo( 'name' ) . '</brand>';

					 $accomm_catalogue .= '<address format="simple">';

							if( get_field('addr1') ) {
								$accomm_catalogue .= '<component name="addr1">' . get_field('addr1') . '</component>';
							}
							if( get_field('city') ) {
								$accomm_catalogue .= '<component name="city">' . get_field('city') . '</component>';
							}
							if( get_field('region') ) {
					      $accomm_catalogue .= '<component name="region">' . get_field('region') . '</component>';
							}
							if( get_field('country') ) {
					      $accomm_catalogue .= '<component name="country">' . get_field('country') . '</component>';
							}
							if( get_field('postcode') ) {
					      $accomm_catalogue .= '<component name="postal_code">' . get_field('postcode') . '</component>';
							}

						$accomm_catalogue .= '</address>';

						if(isset($map)) {

							if( $map['lat'] ) {
						    $accomm_catalogue .= '<latitude>' . $map['lat'] . '</latitude>';
							}
							if( $map['lng'] ) {
						    $accomm_catalogue .= '<longitude>' . $map['lng'] . '</longitude>';
							}

						}

						if( get_field('region') ) {
					    $accomm_catalogue .= '<neighborhood>' . get_field('region') . '</neighborhood>';
						}

				$accomm_catalogue .=
			    '<base_price>' . get_field('min-price') . ' GBP</base_price>' .
			    '<phone>+44' . get_field('company_phone', 'options') . '</phone>';

					// GET STAR RATING
					if ( class_exists( 'Stars_Rating' ) ) {
						$args = array(
							'post_id' => $post_id,
							'status'  => 'approve',
							'parent' => 0
						);
						$comments = get_comments( $args );
						$ratings  = array();
						$count    = 0;
						foreach ( $comments as $comment ) {

							if ( ! empty( $rating ) ) {
								$ratings[] = absint( $rating );
								$count ++;
							}

						}
						if ( 0 != count( $ratings ) ) {
							$avg = round(array_sum( $ratings ) / count( $ratings ), 1);
							$avg_round = round($avg);

							$accomm_catalogue .= '<guest_rating>' .
							'<score>' . $avg . '</score>' .
							'<rating_system>Guest Review</rating_system>' .
							'<number_of_reviewers>' . $count . '</number_of_reviewers>' .
						'</guest_rating>';
						}
					}

					// GET FEATURED IMAGE
					if (has_post_thumbnail()) {
						$accomm_catalogue .= '<image>' .
						 '<url>' . get_the_post_thumbnail_url($post_id,"square_large") . '</url>' .
             '<tag>' . get_the_title() . ' - Holiday Cottage</tag>' .
					 '</image>';
					}


					$accomm_catalogue .= '<url>' . get_permalink() . '</url>' .
				'</listing>';

				endwhile;

				$accomm_catalogue .= '</listings>';

				$fp = fopen( ABSPATH . 'accommodation_catalogue.xml', 'w' );

				fwrite( $fp, $accomm_catalogue );
				fclose( $fp );
		}
}


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_59a580897900a',
	'title' => 'Accommodation - Features',
	'fields' => array(
		array(
			'key' => 'field_59a58092f986a',
			'label' => 'Feature Icon',
			'name' => 'af_ico',
			'type' => 'image',
			'instructions' => 'For best results, image should be a square png with a transparent background',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => 150,
			'min_height' => 150,
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'accommodation_feature',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'modified' => 1556206468,
));

acf_add_local_field_group(array(
	'key' => 'group_59a583664e15b',
	'title' => 'Accommodation - Items',
	'fields' => array(
		array(
			'key' => 'field_5d80d32518e1d',
			'label' => 'Accommodation Details',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5bb76f93e8ef4',
			'label' => 'Accommodation Type',
			'name' => 'accom_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'None' => 'None',
				'Apartment' => 'Apartment',
				'HotelRoom' => 'Hotel Room',
				'House' => 'House',
				'Suite' => 'Suite',
			),
			'default_value' => false,
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5bb775e36fa2b',
			'label' => 'Number of Rooms',
			'name' => 'no_rooms',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '==',
						'value' => 'Apartment',
					),
				),
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '==',
						'value' => 'House',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5bb779232b88b',
			'label' => 'Number of Beds',
			'name' => 'no_beds',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '==',
						'value' => 'HotelRoom',
					),
				),
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '==',
						'value' => 'Suite',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5bb770d7e8ef5',
			'label' => 'Max occupancy',
			'name' => 'max_occupancy',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5d1b2effaa9cb',
			'label' => 'Min Price',
			'name' => 'min-price',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5d1b2f25aa9cc',
			'label' => 'Max Price',
			'name' => 'max-price',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5f2a9061ddfdf',
			'label' => 'Property ID',
			'name' => 'prop_id',
			'type' => 'number',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
		),
		array(
			'key' => 'field_5bb777ac82644',
			'label' => 'Pets',
			'name' => 'pets',
			'type' => 'radio',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'True' => 'Yes',
				'False' => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'False',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
		array(
			'key' => 'field_5d1b3ed2d8c37',
			'label' => 'Does the item have a location?',
			'name' => 'location',
			'type' => 'checkbox',
			'instructions' => 'If you\'d like to include a map to this item, add its location here. The text fields are used in product feeds if you are including your accommodation in a Google or Facebook product catalogue',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5bb76f93e8ef4',
						'operator' => '!=',
						'value' => 'None',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				1 => 'Yes',
			),
			'allow_custom' => 0,
			'default_value' => array(
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
			'save_custom' => 0,
		),
		array(
			'key' => 'field_5e6646b131841',
			'label' => 'Address Line 1',
			'name' => 'addr1',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e6646e831842',
			'label' => 'City',
			'name' => 'city',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e6646fe31843',
			'label' => 'Region',
			'name' => 'region',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e66470b31844',
			'label' => 'Country',
			'name' => 'country',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5e66471931845',
			'label' => 'Post Code',
			'name' => 'postcode',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5d1b3f08d8c38',
			'label' => 'Location',
			'name' => 'map',
			'type' => 'google_map',
			'instructions' => 'Does the item have a location?',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1b3ed2d8c37',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'center_lat' => '53.472225',
			'center_lng' => '-2.2935017',
			'zoom' => '',
			'height' => '',
		),
		array(
			'key' => 'field_5daf64293d96c',
			'label' => 'End accom details',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 1,
		),
		array(
			'key' => 'field_59a5836659fc0',
			'label' => 'Header Settings',
			'name' => 'head_set',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_57694dc34b52a',
			),
			'display' => 'group',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 1,
		),
		array(
			'key' => 'field_59a5836659fd2',
			'label' => 'Accommodation details',
			'name' => 'cont',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layouts' => array(
				'57028dda9a1cc' => array(
					'key' => '57028dda9a1cc',
					'name' => 'plain_text',
					'label' => 'Standard Content',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665dc77',
							'label' => 'Standard',
							'name' => 'standard',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_597600da62b63',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'571ddc3e30011' => array(
					'key' => '571ddc3e30011',
					'name' => 'p_grid',
					'label' => 'Custom Grid',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665dcfb',
							'label' => 'Grid',
							'name' => 'grid',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8508e96484',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'5784fe0ccc983' => array(
					'key' => '5784fe0ccc983',
					'name' => 'recent_grid',
					'label' => 'Auto Populated Grid',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665ddbc',
							'label' => 'Recent',
							'name' => 'Recent',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f899b2e7e8e',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'574315bab87ec' => array(
					'key' => '574315bab87ec',
					'name' => 'two_column',
					'label' => 'Two column text',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665deec',
							'label' => '2col',
							'name' => '2col',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a44d85026',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'571a2d8198c65' => array(
					'key' => '571a2d8198c65',
					'name' => 'image_left',
					'label' => 'Text with image to one side',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665df7c',
							'label' => 'tandi',
							'name' => 'tandi',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a54d331e7',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'571dd8e030b69' => array(
					'key' => '571dd8e030b69',
					'name' => 'tabbed_sections',
					'label' => 'Tabs',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665e0d5',
							'label' => 'Tab',
							'name' => 'tab',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a5b68f833',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'570b8da7ad6f4' => array(
					'key' => '570b8da7ad6f4',
					'name' => 'carousel',
					'label' => 'Slideshow',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665e12d',
							'label' => 'slideshow',
							'name' => 'slideshow',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a677d5936',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'571a2e6f98c6d' => array(
					'key' => '571a2e6f98c6d',
					'name' => 'media_element',
					'label' => 'External Media (Youtube/Soundcloud/ect.)',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665e16a',
							'label' => 'media',
							'name' => 'media',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a6c16c840',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'581727f028ca4' => array(
					'key' => '581727f028ca4',
					'name' => 'gallery',
					'label' => 'Gallery',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a58c29e91ea',
							'label' => 'Gal',
							'name' => 'gal',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f8a7089d252',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'59a583f5bc044' => array(
					'key' => '59a583f5bc044',
					'name' => 'feat_list',
					'label' => 'Include Feature list',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59a583665e1d5',
							'label' => 'tags',
							'name' => 'tags',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f98ff7a14f6',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'59b91d5b2a8e2' => array(
					'key' => '59b91d5b2a8e2',
					'name' => 'cust_feat_list',
					'label' => 'Custom Feature List',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_59b8f652cbf43',
							'label' => 'features',
							'name' => 'features',
							'type' => 'clone',
							'instructions' => 'Add a short description so this slice can be found easier amongst others',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_59f992a311c93',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				'5ab374c485f43' => array(
					'key' => '5ab374c485f43',
					'name' => 'slice_library',
					'label' => 'Add from Slice Library',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_5ab374e785f44',
							'label' => 'Slice Library',
							'name' => 's_lib',
							'type' => 'clone',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'clone' => array(
								0 => 'group_5ab289aba8bcd',
							),
							'display' => 'seamless',
							'layout' => 'block',
							'prefix_label' => 0,
							'prefix_name' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
			),
			'button_label' => 'Add Slice',
			'min' => 1,
			'max' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'accom',
			),
		),
	),
	'menu_order' => 1,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'field',
	'hide_on_screen' => array(
		0 => 'the_content',
	),
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_59f98ff7a14f6',
	'title' => 'Accommodation - Slice - Tag Features',
	'fields' => array(
		array(
			'key' => 'field_59f98ff7e223c',
			'label' => 'Slice Name',
			'name' => 'sname',
			'type' => 'text',
			'instructions' => 'Add a short description so this slice can be found easier amongst others [don\'t use numbers as this can affect the styling of the site]',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Feature List',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_59f98ff7e224b',
			'label' => 'Content',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5a018cc8cd356',
			'label' => 'Intro',
			'name' => 'intro',
			'type' => 'wysiwyg',
			'instructions' => 'The content of this area will appear above the list of features. Leave this blank if not needed.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 0,
		),
		array(
			'key' => 'field_59f98ff7e2262',
			'label' => 'Feature list now included',
			'name' => '',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => 'Add features with the checkboxes in the sidebar.
To add an icon to the feature, visit it in the <a href="/wp-admin/edit-tags.php?taxonomy=accommodation_tags&post_type=accom">features section</a> inside the Accommodation area.',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		array(
			'key' => 'field_59f98ff7e227c',
			'label' => 'Spacing',
			'name' => 'spacing',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_595ba7b3968e6',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_59f98ff7e228c',
			'label' => 'Overlay',
			'name' => 'overlay',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_590b2b9b33fb8',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_59f98ff7e22c7',
			'label' => 'Slice Background',
			'name' => 'slice_background',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_58fdf8606236d',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'accom',
			),
		),
	),
	'menu_order' => 1,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'field',
	'hide_on_screen' => array(
		0 => 'the_content',
	),
	'active' => false,
	'description' => '',
	'modified' => 1556206842,
));

acf_add_local_field_group(array(
	'key' => 'group_59b1229808372',
	'title' => 'Accommodation - Type',
	'fields' => array(
		array(
			'key' => 'field_59b122add2b18',
			'label' => 'Options',
			'name' => 'options',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_596dd8a04c538',
				1 => 'field_57064501f7ef9',
				2 => 'field_570649f2fb9bc',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_59b7c83b4c367',
			'label' => 'Order',
			'name' => 'order',
			'type' => 'number',
			'instructions' => 'Add a number here to sort the room types by: the lower the number the higher on the page eg. 1 = top of page. **TIP** If you foresee adding extra room types in the future use multiples of ten initially, then you have 9 spaces between each type to add additional types into.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => 1,
		),
		array(
			'key' => 'field_5d1b3e6efbb41',
			'label' => 'Include a map?',
			'name' => 'include_map',
			'type' => 'radio',
			'instructions' => 'If you have added location information to the items in this accommodation type you can have them display in a map at the top of this accommodation type view',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'yes' => 'Yes',
				'no' => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'no',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'accommodation_type',
			),
		),
	),
	'menu_order' => 29,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'modified' => 1562066616,
));

endif;
