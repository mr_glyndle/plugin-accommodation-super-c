<style>
	.gmap .gm-style .gm-style-iw-c {
			max-width: 30em !important;
	}
	.gmap .gm-style .gm-style-iw-d img,
	.gmap .gm-style .gm-style-iw-d .text {
		display: inline-block;
		vertical-align: top;
	}
	.gmap .gm-style .gm-style-iw-d img {
		width: 30%;
	}
	.gmap .gm-style .gm-style-iw-d .text {
		width: 69%;
		padding: 0 1em;
	}
	.gmap .gm-style .gm-style-iw-d .text .address {
		padding-top: 0;
	}
</style>
	<?php echo '<div class="gmap">';

	while ( $query->have_posts() ) : $query->the_post();

	unset($prop_id);
	$prop_id = get_field('prop_id');
	$min_price = get_field('min-price');

		$map = get_field('map');

		if( !empty($map) ) {

			echo '<div ';
				if($prop_id !== '') { echo 'id="ID'.$prop_id.'"'; }
			echo 'class="marker" data-lat="'. $map['lat'] . '" data-lng="' . $map['lng'] . '">';

			if (has_post_thumbnail()) {
				echo '<a href="' . get_permalink() . '">';
				the_post_thumbnail('square', array('class' => 'lazy'));
				echo '</a>';
			} else {
				echo '<img class="placeholder" src="' . get_template_directory_uri() . '/img/placeholder.png" alt="' . get_the_title() . '">';
			}
			echo '<div class="text">';
			echo '<h5><a href="' . get_permalink() . '">' .  get_the_title() . '</a></h5>';
			if ($min_price) {
				echo '<p style="padding-top:0;">From £' . $min_price . ' per week</p>';
			};
			echo '<p class="address">' . get_the_excerpt() . '<a href="' . get_permalink() . '"> - More info</a></p>';
			echo '</div></div>';

		}

	endwhile;

	echo '</div>';

	include( plugin_dir_path( __FILE__ ) . '/map-script.php');
