<?php
include( plugin_dir_path( __FILE__ ) . '/map-script.php');
$map = get_field('map');
if( !empty($map) ) { ?>

	<div class="gmap" style="height: 400px">
		<div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>">
			<p style="padding: 0; text-align: center;"><a class="btn inline-block vmiddle" style="padding:.5em;" target="blank" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo $map['lat']; ?>,<?php echo $map['lng']; ?>&travelmode=driving">Get Directions</a> <span class="inline-block vmiddle" style="width: 18em; padding: .5em; text-align: left;">Please note, this marker may not be 100% accurate to the location of the property.</span></p>
		</div>
	</div>

<?php } ?>
