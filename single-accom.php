<?php get_header();
$maps_api = get_field('maps_api', 'option');
echo '<script src="https://maps.googleapis.com/maps/api/js?key='.$maps_api.'"></script>';
if (have_posts()) {

	// vars
	$prop_id = get_field('prop_id');
	$accom_type = get_field('accom_type');
	$max_occupancy = get_field('max_occupancy');
	$no_beds = get_field('no_beds');
	$no_rooms = get_field('no_rooms');
	$max_price = get_field('max-price');
	$min_price = get_field('min-price');
	$pets = get_field('pets');
	$schema_title = get_the_title();
	$schema_blog_name = get_bloginfo( 'name' );
	$map = get_field('map');


	$GLOBALS['footer-schema'] .= '"@context": "http://schema.org","@type": ["' . $accom_type . '", "Product"],
	"name": "' . $schema_title . '"';

	if(get_permalink()) {
		$GLOBALS['footer-schema'] .= ',
		"url": ' . json_encode(get_permalink()) . ',
		"@id": "' . get_permalink() . '#accommodation"';
	}
	if($prop_id !== '') {
		$GLOBALS['footer-schema'] .= ',
		"sku": "'.$prop_id.'"';
	}
	$GLOBALS['footer-schema'] .= ',
	"description": ' . json_encode(get_the_excerpt());

	$GLOBALS['footer-schema'] .= ',
	"brand": {
		"@type" : "http://schema.org/Brand",
		"name": "' . $schema_blog_name . '"
	}';

	if( $accom_type == 'HotelRoom' || $accom_type == 'Suite' ) {
		$GLOBALS['footer-schema'] .= ',
		"bed": {
			"@type" : "BedDetails",
			"numberOfBeds" : "' . $no_beds . '"
		}';
	} else {
		$GLOBALS['footer-schema'] .= ',
		"numberOfRooms" : "' . $no_rooms . '"';
	}

	if ($accom_type !== 'House') {
		 $GLOBALS['footer-schema'] .= ',
		 "occupancy": {
			 "@type" :
			 	"QuantitativeValue",
			 	"maxValue" : "' . $max_occupancy . '",
				"unitCode" : "C62"
			}';
	}
	if ($min_price !== '') {
		$GLOBALS['footer-schema'] .= ',
		"offers": {
			"@type": "AggregateOffer",
			"lowPrice": "' . $min_price . '",
			"highPrice": "' . $max_price . '",
			"priceCurrency": "GBP",
			"offerCount": "1"
		}';
	}

	$GLOBALS['footer-schema'] .= ',
	"petsAllowed": "' . $pets . '"';

	if(get_the_post_thumbnail_url()) {
		$GLOBALS['footer-schema'] .= ',
		"image": {
			"@type": "imageObject",
			"url": ' . json_encode(get_the_post_thumbnail_url(get_the_ID(),'golden_medium')) . ',
			"height": "632px",
			"width": "898px"
		}';
	}
	if( !empty($map) ) {
		$GLOBALS['footer-schema'] .= ',"geo": {
		    "@type": "GeoCoordinates",
		    "latitude": " ' . $map['lat'] . '",
		    "longitude": "' . $map['lng'] . '"
		}';
	}

	while (have_posts()) : the_post(); ?>

		<?php // check if the flexible content field has rows of data
    	if( have_rows('cont') ) {

			$item_count = 1;
			// loop through the rows of data
			while ( have_rows('cont') ) : the_row();

				// Make these reusable functions rather than repeatedly calling files?
				include(locate_template('partials/slice_loop.php'));


			endwhile;


			// if (isset($prop_id) && $prop_id !== '') {
			//
			// 	echo '<div id="Availability" class="slice text Availability dark "><div class="content txt_blk s_over  avs_default  avm_default nopad_bottom nopad_left nopad_right nopad_full_width  normal"><div class="text_content"><h2 style="text-align: center;">Check Availability</h2><p class="small" style="text-align: center;"><strong>1.</strong> Select dates on calendar &gt;&nbsp;<strong>2.</strong> Select number of guests &gt;&nbsp;<strong>3.</strong> Select number of nights</p><p>&nbsp;</p><div data-calendar-key="166C8CAEDD57223D18AD3F87B7F70117B247792B5BD095917A794341F65D5167109E26D13D5F774EA9F6E930C345EFE1DF61D902700CB056" data-calendar-property-id="' . $prop_id . '" data-calendar-widescreen-months="2">Availablity Calendar Loading...</div><script src="https://secure.supercontrol.co.uk/components/embed.js"></script></div></div></div>';
			//
			// }

			// Is there a map for this item?
			$location = get_field('map');
			if (isset($location) && $location !== '') {
				include( plugin_dir_path( __FILE__ ) . '/map/single-map.php');
			}

			} else { ?>
				<div class="txt_blk"><?php the_content();?></div>
		 	<?php };
		endwhile;
	} else { ?>

		<div class="txt_blk">
			<div class="alert alert-info">
				<h1>Sorry, we can't find the page you're looking for</h1>
				<p>Please use the navigation or, seach the site with the options in the menu.</p>
			</div>
		</div>

	<?php };

	if (comments_open() || get_comments_number()) { ?><div class="txt_blk comments"><?php comments_template(); ?></div><?php };

get_footer();
