<?php
	get_header(); $site_width = get_field('site_width', 'option');
	$maps_api = get_field('maps_api', 'option');
	echo '<script src="https://maps.googleapis.com/maps/api/js?key='.$maps_api.'"></script>';
	echo '<script>window.onpopstate = function(event) {
			window.location.reload();
	};</script>';

	// Load svgs
	include( plugin_dir_path( __FILE__ ) . 'accom/svgdefs.php');?>

	<section id="accommodation" class="category accom <?php if ($site_width == 'full') { echo " full"; };?> clearfix">
		<script type="application/ld+json">{
			"@context": "http://schema.org"
			,"@type": "Hotel"
			<?php if(get_the_post_thumbnail_url()) {
				echo ',"image": {
						"@type": 	"imageObject"
						,"url": 	' . json_encode(get_the_post_thumbnail_url(get_the_ID(),'golden_medium')) . '
						,"height": 	"632px"
						,"width": 	"898px"
				},';
			} else if (get_field('company_image', 'option')) {
				echo ',"image": {
						"@type": 	"imageObject"
						,"url": 	"' . get_field('company_image', 'option') . '"
						,"height": 	"632px"
						,"width": 	"898px"
				},';
		    }?>
			"name": 		"<?php single_cat_title(); ?>"
			,"telephone" : 	"+44<?php echo get_field('company_phone', 'options') ; ?>" // change the country code
			,"address" : {
	            "@type" 			:	"PostalAddress"
	            ,"streetAddress"	:	"<?php echo get_field('address_street', 'option'); ?>"
	            ,"postalCode" 		:	"<?php echo get_field('address_postal', 'option'); ?>"
	            ,"addressLocality"	:	"<?php echo get_field('address_locality', 'option'); ?>"
	            ,"addressRegion"	:	"<?php echo get_field('address_region', 'option'); ?>"
	            ,"addressCountry"	:	"<?php echo get_field('address_country', 'option'); ?>"
	        }
			<?php if (get_field('pricerange_min', 'option')) { ?>
			,"priceRange"	:	"£<?php echo get_field('pricerange_min', 'option'); ?> - £<?php echo get_field('pricerange_max', 'option'); ?>"
			<?php } ?>
		}</script>
		<?php

			$current_url_params = $_SERVER['REQUEST_URI'];
			$url_parts = explode('/', $current_url_params);
			$filters['location'] = $url_parts[2];

			if ( strpos($current_url_params, '?') ) {
				// Current_url_params contains a "?"

				if (strpos($current_url_params, 'accommodation_feature')) {

					// Current_url_params contains "accommodation_feature" - Nead to deal with standard wordpress URL structure
					$filters['extras'] = substr($current_url_params, strpos($current_url_params, "=") + 1);

				} else {

					// Using plugin sepecific URL
					$current_url_params = explode('?', $current_url_params);
					foreach ($current_url_params as $filter) {
						if ( strpbrk($filter, '=') ) {
							$filter = explode('=', $filter);
							$filters[$filter[0]] = $filter[1];
						}
					}
				}

			} else {

				// Current_url_params does not contains a "?"
				$location = explode('/', $current_url_params);
				if (!empty($location)) {
					$filters['location'] = $location[2];
				}

			}


		if( !empty($filters) ) {

			// Sort by title-date-random
			if ( isset($filters['sortby']) && $filters['sortby'] !== 'price' ) {

				$args = array(
					'orderby' => $filters['sortby'],
					'order'		=> $filters['order']
				);

			// or sort by price
			} else if (isset($filters['sortby']) && $filters['sortby'] == 'price') {

				// Sort price acsending
				if ( $filters['order'] == 'ASC' ) {

					$args = array(
						'post_type'	=> 'accom',
						'meta_key'	=> 'min-price',
						'orderby' 	=> 'meta_value_num',
						'order'			=> 'ASC' // ASC or DESC
					);

				// Sort price descending
				} else {
					$args = array(
						'post_type'	=> 'accom',
						'orderby' 	=> 'meta_value_num',
						'meta_key' 	=> 'min-price',
						'order'			=> 'DESC' // ASC or DESC
					);
				}
			}


			if( !empty( $filters['location'] ) || !empty($filters['extras']) ) {


				// Filter by both category and tags
				if( !empty( $filters['location'] ) && !empty($filters['extras']) ) {

					$locationArray = explode(',', $filters['location']);
					$extrasArray = explode('+', $filters['extras']);

					$args['tax_query'] = array(
						'post_type' => 'accom',
						'relation' => 'AND',
						array(
							'taxonomy'					=> 'accommodation_type',
							'field' 						=> 'slug',
							'include_children'	=> 1,
							'terms' 						=> $locationArray,
						),
						array(
							'taxonomy'	=> 'accommodation_feature',
							'field' 		=> 'slug',
							'terms'			=> $extrasArray,
							'operator' 	=> 'AND',
						)
					);

				// Filter just by location
				} else if( !empty( $filters['location'] ) ) {
					$locationArray = explode(',', $filters['location']);
					$args['tax_query'] = array(
						'post_type' => 'accom',
						array(
							'taxonomy'					=> 'accommodation_type',
							'include_children'	=> 1,
							'field' 						=> 'slug',
							'terms' 						=> $locationArray
						)
					);

				// Filter just by tags
				} else if( $filters['extras'] !== '' ) {
					$extrasArray = explode('+', $filters['extras']);
					$args['tax_query']	= array(
						'post_type' 			=> 'accom',
						array(
							'taxonomy'			=> 'accommodation_feature',
							'field' 				=> 'slug',
							'terms'					=> $extrasArray,
							'operator' 			=> 'AND',
						)
					);
				}
			}



			// Ensure Price min & max are intergers
			if( isset( $filters['price_min'] ) && $filters['price_min'] ) {
				$filters['price_min'] = $filters['price_min'] + 0;
			}
			if ( isset( $filters['price_max'] ) && $filters['price_max'] ) {
				$filters['price_max'] = $filters['price_max'] + 0;
			}


			// create $args['meta_query'] array if one of the following fields is filled
			if(
				isset( $filters['price_min'] ) && $filters['price_min'] ||
				isset( $filters['price_max'] ) && $filters['price_max'] ||
				isset( $filters['max_occupancy'] ) && $filters['max_occupancy'] ||
				isset( $filters['room'] ) && $filters['room'] ) {
					$args['meta_query'] = array( 'relation'=>'AND' ); // AND means that all conditions of meta_query should be true
				}

			// if both minimum price and maximum price are specified we will use BETWEEN comparison
			if( isset( $filters['min-price'] ) && $filters['min-price'] && isset( $filters['max-price'] ) && $filters['max-price'] ) {
				$args['meta_query'][] = array(
					'relation' => 'AND',
					array (
						'key' => 'min-price',
						'value' => $filters['min-price'],
						'type' => 'numeric',
						'compare' => '>='
					),
					array (
						'key' => 'min-price',
						'value' => $filters['max-price'],
						'type' => 'numeric',
						'compare' => '<'
					)
				);

			} else {
				// if only min price is set
				if( isset( $filters['min-price'] ) && $filters['min-price'] ) {
					$args['meta_query'][] = array(
						'key' => 'min-price',
						'value' => $filters['min-price'],
						'type' => 'numeric',
						'compare' => '>='
					);
				};

				// if only max price is set
				if( isset( $filters['max-price'] ) && $filters['max-price'] ) {
					$args['meta_query'][] = array(
						'key' => 'min-price',
						'value' => $filters['max-price'],
						'type' => 'numeric',
						'compare' => '<'
					);
				}
			}

			if(isset( $filters['max_occupancy'] ) && $filters['max_occupancy'] ) {
				$args['meta_query'][] = array(
					'key' => 'max_occupancy',
					'value' => $filters['max_occupancy'],
					'type' => 'numeric',
					'compare' => '>='
				);
			}
			if(isset( $filters['room'] ) && $filters['room'] ) {
				$args['meta_query'][] = array(
					'key' => 'no_rooms',
					'value' => $filters['room'],
					'type' => 'numeric',
					'compare' => '>='
				);
				// Append Max occupancy to URL
			}

		} else if ( !empty($location) ) {

			$args['tax_query'] = array(
				'post_type' => 'accom',
				array(
					'taxonomy'					=> 'accommodation_type',
					'include_children'	=> 1,
					'field' 						=> 'slug',
					'terms' 						=> $location
				)
			);

		} else {

			$args = array(
				'post_type' => 'accom',
				'orderby' => 'rand',
				'order'    => 'ASC',
				'posts_per_page' => '-1'
			);

		}

		// Load filters
		include( plugin_dir_path( __FILE__ ) . 'accom/filters.php');

		?>
		<div class="sub_cat grid_cont">
			<div class="grid  card  full">
				<div id='response'>

					<?php

					// print_r($args);
					$query = new WP_Query( $args );

					if( $query->have_posts() ) {

						$grid_item_count = 1;

						if ( isset($filters['viewtype']) && $filters['viewtype'] == 'map') {
							include( plugin_dir_path( __FILE__ ) . '/map/multi-map.php');
						} else {

							echo '<div class="items" style="justify-content: center;">';

	            // Start the Loop.
	            while ( $query->have_posts() ) : $query->the_post();

								include(plugin_dir_path( __FILE__ ) . 'accom/card_view.php');

							endwhile;

							echo '</div>';
						}
					}?>

				</div>
			</div>
		</div>

</section>

<?php get_footer(); ?>
