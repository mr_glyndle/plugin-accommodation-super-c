<?php // vars
unset($prop_id);
$accom_type = get_field('accom_type');
$max_occupancy = get_field('max_occupancy');
$max_price = get_field('max-price');
$min_price = get_field('min-price');
$prop_id = get_field('prop_id');
$no_beds = get_field('no_beds');
$no_rooms = get_field('no_rooms');
$pets = get_field('pets');
$map = get_field('map');?>

<div <?php if($prop_id !== '') { echo 'id="ID'.$prop_id.'"'; } ?> class="item ic_<?php echo $grid_item_count; ++$grid_item_count;?>">
	<div class="txt_bg"></div>
	<a class="thumbnail_link image_container" href="<?php echo get_permalink(); ?>">
		<?php if (has_post_thumbnail()) {
			the_post_thumbnail('golden_medium', array('class' => 'lazy'));
		} else {
			echo '<img class="placeholder" src="' . get_template_directory_uri() . '/img/placeholder.png" alt="' . get_the_title() . '">';
		}?>
		<?php if ($min_price) {
						echo '<p class="small price"><span class="from">From </span><span class="cost">£' . $min_price . '</span><span class="time">Per week</span></p>';
					};
					if($prop_id) {
						echo '<div id="availability' . $prop_id . '" class="avdate"></div>'; ?>
			<script>

				var startDate = '';
				startDate = document.getElementById("arrival-date").value;

				if(startDate !== '') {

					var dds = new Date(startDate);

					dds.setDate(dds.getDate() - 2);
					startyear = dds.getFullYear();
					startmonth = dds.getMonth()+1;
					startday = dds.getDate();

					if (startday < 10) {
						startday = '0' + startday;
					}
					if (startmonth < 10) {
						startmonth = '0' + startmonth;
					}

					startDate = startyear+'-' + startmonth + '-'+startday;

					var dds = dds.getDate();
					if (dds < 10) {
						dds = '0' + dds;
					}

					var endDate = new Date(startDate);
					endDate.setDate(endDate.getDate() + 4);

					var dd = endDate.getDate();
					if (dd < 10) {
						dd = '0' + dd;
					}

					var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
					var mm = endDate.getMonth();
					var monthName = months[mm];
					var mm = mm + 1;
					if (mm < 10) {
						mm = '0' + mm;
					}

					var y = endDate.getFullYear();
					var endDate = y + '-' + mm + '-' + dd;

					var xhr<?php echo $prop_id; ?> = new XMLHttpRequest();
					xhr<?php echo $prop_id; ?>.open("GET", "https://api.supercontrol.co.uk/xml/property_avail.asp?propertycode=<?php echo $prop_id; ?>&startdate="+startDate+"&enddate="+endDate+"", true);

					xhr<?php echo $prop_id; ?>.onload = function(){
						if(this.readyState == 4 && this.status == 200) {
							availabiltyCheck(this);
						}
					};
					xhr<?php echo $prop_id; ?>.send();
				} else {
					$(".avdate").hide();
				}

				// Update availability indicator on card
				function availabiltyCheck(xml) {

						var xmlDoc = xml.responseXML;

						txt = "";
						id = xmlDoc.getElementsByTagName('propertycode')[0].childNodes[0].nodeValue;

						dates = xmlDoc.getElementsByTagName('date');

						var i = 0;
						avtext = '';
						var dds = new Date(startDate);
						dds.setDate(dds.getDate() - 1);

						for (;dates[i];) {

							dds.setDate(dds.getDate() + 1);
							startdaynumber = dds.getDate();

							if (startdaynumber < 10) {
								startdaynumber = '0' + startdaynumber;
							}

							var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
							var mm = dds.getMonth();
							var monthName = months[mm];



							var propertyID = document.getElementById('availability'+id);

							// If the date is not a valid arrival day
							if(i < 6) {

								// Make container for availability
								avtext += "<div id='availability" + id + i + "' class='";
								// If we can find the arrival permissions, add it as a class
								if( typeof dates[i].getAttribute('allowedarrival') !== 'undefined') {
									avtext += dates[i].getAttribute('allowedarrival') + " ";
								}
								// If we can find the booking status, add it as a class
								if( typeof dates[i].getAttribute('status') !== 'undefined') {
									avtext += dates[i].getAttribute('status') + " ";
								}
								// Close starting div
								avtext += "'>";


								// Add the date
								avtext += "<span class='day'>" + startdaynumber + "</span><span class='month'>" + monthName + "</span>";

								// Add text to show availability and arrival permissions - close the div
								avtext += "<span class='status'>";

								if( typeof dates[i].getAttribute('allowedarrival') !== 'undefined') {
									if (dates[i].getAttribute('allowedarrival') == 'false') {
										avtext += "No Arrivals</ br>";
									} else if( typeof dates[i].getAttribute('status') !== 'undefined') {
											avtext += dates[i].getAttribute('status') + " ";
									}
								} else if( typeof dates[i].getAttribute('status') !== 'undefined') {
									avtext += dates[i].getAttribute('status') + " ";
								}
								avtext +=	"</span></div>";

							}
							i++;
						}
						propertyID.insertAdjacentHTML("afterbegin", avtext);
				}

			</script>
		<?php } ?>
	</a>
	<div class="text">
		<div class="text_content">
			<h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
			<p class="small"><?php echo excerpt(55); ?></p>


			<?php
			$currentID = get_the_ID();
			$accom_tags = get_the_terms( $currentID, 'accommodation_feature');

				echo '<p class="small tags">';
				if ($pets == "True") {
					echo 'Pet Friendly | ';
				}
				if($no_rooms) {
					echo '<b>'.$no_rooms . '</b> bedroom';
					if ($no_rooms > 1) {
						echo 's';
					}
					echo ' | ';
				}
				if ($no_beds) {
					echo '<b>'.$no_beds.'</b> bed';
					if ($no_beds > 1) {
						echo 's';
					}
					echo ' | ';
				}
				if($max_occupancy) {
					echo 'Up to <b>' . $max_occupancy . '</b> guests';
				}
				echo '</p>';

			if ( get_post_status() == 'private' ) {
				show_publish_button();
			};?>
			<p class="small link"><a href="<?php echo get_permalink(); ?>" class="btn">More...</a></p>
		</div>
	</div>
	<?php if(!empty($accom_type)) { if($accom_type !== 'None') {?>
	<script type="application/ld+json"> {
		"@context": "http://schema.org"
		,"@type": ["<?php echo $accom_type ?>", "Product"]
		,"name": "<?php echo get_the_title(); ?>"
		,"@id": "<?php echo get_permalink() . '#accommodation'; ?>"
		<?php if($prop_id !== '') {
			echo ',	"sku": "'.$prop_id.'"';
		} ?>
		,"description": "<?php echo htmlspecialchars(get_the_excerpt(), ENT_QUOTES); ?>"

		<?php if(get_permalink()) { echo ', "url": ' . json_encode(get_permalink()) . ','; }; ?>
		"brand": {
			"@type" : "http://schema.org/Brand"
			,"name": "<?php echo bloginfo( 'name' ); ?>"
		}
		<?php if ($accom_type == 'HotelRoom' || $accom_type == 'Suite') { ?>
		,"bed": {
			"@type" : "BedDetails"
			,"numberOfBeds" : "<?php echo $no_beds; ?>"
		}
		<?php } else { ?>
		,"numberOfRooms" : "<?php echo $no_rooms; ?>"
		<?php }
		if ($accom_type !== 'House') {
			echo ',"occupancy": {
				"@type" : "QuantitativeValue"
				,"maxValue" : "' . $max_occupancy . '"
				,"unitCode" : "C62"
			}';
		}
		if ($min_price !== '') {
			echo ',"offers": {
				"@type": "AggregateOffer",
				"offerCount" : "1",
				"lowPrice": "' . $min_price . '",
				"highPrice": "' . $max_price . '",
				"priceCurrency": "GBP"
			}';
		}
		if( !empty($map) ) {
			echo ',"geo": {
			    "@type": "GeoCoordinates",
			    "latitude": " ' . $map['lat'] . '",
			    "longitude": "' . $map['lng'] . '"
			}';
		}

		if ( class_exists( 'Stars_Rating' ) ) {
			$args = array(
				'post_id' => $currentID,
				'status'  => 'approve',
				'parent' => 0
			);
			$comments = get_comments( $args );
			$ratings  = array();
			$count    = 0;
			foreach ( $comments as $comment ) {

				if ( ! empty( $rating ) ) {
					$ratings[] = absint( $rating );
					$count ++;
				}

			}
			if ( 0 != count( $ratings ) ) {
				$avg = round(array_sum( $ratings ) / count( $ratings ), 1);
				$avg_round = round($avg);

				echo ',
				"aggregateRating": {
					"@type": "AggregateRating",
					"ratingValue": "' . $avg . '",
					"ratingCount": "' . $count . '"
				}';
			}
		}
		if (has_post_thumbnail()) {
			echo ',"image": ' . json_encode(get_the_post_thumbnail_url($currentID,'flex_height_small'));
		} else {
			echo ',"image": "' . get_template_directory_uri() . '/img/placeholder.png"';
		}
		if ($pets == "True") {?>
			,"petsAllowed": "<?php echo $pets; ?>"
		<?php } ?>
		<?php if (!empty($accom_tags)) {
			echo ',"amenityFeature": [';
			$count = count( $accom_tags );
			$current = 0;
			foreach ( $accom_tags as $tag ) {
				echo '{ "@type" : "LocationFeatureSpecification", "value": "True", "name": "'. $tag->name . '"}';
				++$current;
				if ($current < $count) {
					echo ', ';
				} else {
					echo ']}';
				}
			}
		} ?>
	}
	</script>
<?php } }?>
</div>
